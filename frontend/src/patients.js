import React, { Component } from 'react';
import { useTranslation, withTranslation, Trans } from 'react-i18next';

import { USA } from './provinces';
import { CA } from './provinces';
import {validatePostalCode} from './validators';


class Patients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '',
      lastName: '',
      email: '',
      country: 'Canada',
      province: CA[0],
      provinceList: CA,
      postalCode: '',
      postalClass: 'form-control',
      lowSystolic: '0',
      lowSystolicClass: 'form-control',
      highSystolic: '0',
      highSystolicClass: 'form-control',
      patients: [],
      formStatus: '',
      formStatusClass: '',
    };
    
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleFirstNameChange = this.handleFirstNameChange.bind(this);
    this.handleLastNameChange = this.handleLastNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleCountryChange = this.handleCountryChange.bind(this);
    this.handleProvinceChange = this.handleProvinceChange.bind(this);
    this.handlePostalCodeChange = this.handlePostalCodeChange.bind(this);
    this.handleLowSystolicChange = this.handleLowSystolicChange.bind(this);
    this.handleHighSystolicChange = this.handleHighSystolicChange.bind(this);

  }
  
  componentDidMount(){
    let patients = [];
    if(this.props.patients){
      patients = this.props.patients; 
    }
    this.setState({
      patients: patients
    })
  }


  deletePatient(id, event) {
    const { patientService } = this.props;
    console.log('deleting patientd id: ' + id) ;
    console.table(id); 
    patientService.remove(id)
    let patients = this.state.patients;
    console.table(patients);
    let newPatients = patients.filter( newPatient => newPatient.id !== id);
    console.table(newPatients);
    this.setState({
      patients: newPatients
    })
  }
  
  handleSubmit(event) {
    const { patientService } = this.props;
    
    patientService.create({
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
      country: this.state.country,
      province: this.state.province,
      postalCode: this.state.postalCode,
      lowSystolic: this.state.lowSystolic,
      highSystolic: this.state.highSystolic,
    })
    .then( result => {
      let patients = this.state.patients;
      patients.push(result);
      this.setState({ 
        patients: patients,
        formStatus: 'Pattient added succesfully',
        formStatusClass: 'form-control is-valid'
      });
      
      setTimeout( () => {
          this.setState({
             firstName: '',
             lastName: '',
             email: '',
             country: 'Canada',
             province: CA[0],
             provinceList: CA,
             postalCode: '',
             postalClass: 'form-control',
             lowSystolic: '0',
             lowSystolicClass: 'form-control',
             highSystolic: '0',
             highSystolicClass: 'form-control',
             formStatus: '',
             formStatusClass: '',
          })  
      },1500);
      
    })
    .catch( error => {
      this.setState({
        formStatus: error.message,
        formStatusClass: 'form-control is-invalid'
      });
    });

    event.preventDefault();
  }
  
  handleEmailChange(event){
    this.setState({email: event.target.value});
  }
  
  handleFirstNameChange(event){
    this.setState({firstName: event.target.value});
  }
  
  handleLastNameChange(event){
    this.setState({lastName: event.target.value});
  }
  
  handleCountryChange(event){
    let provinceList = [];
    let updatedProvince = '';
    if(event.target.value === 'USA'){
       provinceList = USA;
    }else {
      provinceList = CA;
    }
    updatedProvince = provinceList[0];
    this.setState({country: event.target.value, provinceList: provinceList, province: updatedProvince, postalCodeValid: false, postalCode: ''});
  }
  
  handleProvinceChange(event){
    this.setState({province: event.target.value});
  }
  
  handlePostalCodeChange(event){
    let postalClass = 'form-control'; 
    if(!validatePostalCode(this.state.country, event.target.value)){
      postalClass += ' is-invalid'  
    }
    this.setState({postalCode: event.target.value, postalClass: postalClass});
  }
 
  handleLowSystolicChange(event){
    let systolicClass = 'form-control'; 
    if(event.target.value > Number(this.state.highSystolic)){
      systolicClass +=  ' is-invalid';
    }
    this.setState(
      {lowSystolic: event.target.value,
      lowSystolicClass: systolicClass,
      highSystolicClass: systolicClass
    });
  }
  
  handleHighSystolicChange(event){
    let systolicClass = 'form-control';
    if(event.target.value < Number(this.state.lowSystolic)){
      systolicClass += ' is-invalid';
    }
    this.setState({
      highSystolic: event.target.value, 
      highSystolicClass: systolicClass,
      lowSystolicClass: systolicClass,
    });
  }

  render() {
    
    const { t, patients} = this.props;
    
    let options = this.state.provinceList.map( province => {
      return <option value={province} key={province}>{province}</option>
    });
    
    return(
    <div>
      <div className="py-5 text-center">
        <h2>{t('title')}</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.handleSubmit} 
                className={this.state.formclass} 
                id="carform">
            
            <div className="form-group">
                <label htmlFor="formGroupExampleInput">{t('labels.firstName')}</label>
                <input  type="text" 
                        className="form-control" 
                        id="formGroupExampleInput" 
                        placeholder="Enter your first name..." 
                        value={this.state.firstName}
                        onChange={this.handleFirstNameChange}
                        required
                        />
            </div>
            
            <div className="form-group">
              <label htmlFor="formGroupExampleInput">{t('labels.lastName')}</label>
              <input  type="text"
                      className="form-control" 
                      id="formGroupExampleInput" 
                      placeholder="Enter your last name..." 
                      value={this.state.lastName}
                      onChange={this.handleLastNameChange}
                      required
                      />
            </div>
            
            <div className="form-group">
              <label htmlFor="email">{t('labels.email')}:</label>
              <input  type="email" 
                      className="form-control" 
                      id="email" 
                      aria-describedby="emailHelp" 
                      placeholder="Enter your email..."
                      value={this.state.email}
                      onChange={this.handleEmailChange}
                      required
                      />
               <div id="email-valid" className="invalid-feedback"></div>
            </div>
            
            
           <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">{t('labels.country')}:</label>
              <select className="form-control" 
                      id="exampleFormControlSelect1"
                      value={this.state.country}
                      onChange={this.handleCountryChange}
                      required
                >
                  <option>Canada</option>
                  <option>USA</option> 
              </select>
            </div>
            
           <div className="form-group">
              <label htmlFor="exampleFormControlSelect1">{t('labels.province')}:</label>
              <select className="form-control" 
                      id="exampleFormControlSelect1"
                      value={this.state.province}
                      onChange={this.handleProvinceChange}
                      >
              {options}
                
              </select>
            </div>
            
            <div className="form-group">
              <label htmlFor="postalCode">{t('labels.postalCode')}:</label>
              <input  type="text" 
                      className={this.state.postalClass}
                      id="lowSystolic" 
                      aria-describedby="emailHelp" 
                      placeholder="Zip Code.."
                      value={this.state.postalCode}
                      onChange={this.handlePostalCodeChange}
                      
                      />
                <div className="invalid-feedback">
                  You must enter a valid postal code for the selected country.
                </div>
            </div>
            
            <div className="form-group">
              <label htmlFor="lowSystolic">{t('labels.lowSystolic')}:</label>
              <input  type="number" 
                      className={this.state.lowSystolicClass}
                      id="lowSystolic" 
                      value={this.state.lowSystolic}
                      onChange={this.handleLowSystolicChange}
                      />
                <div className="invalid-feedback">
                {t('errors.lowPressure')}
                </div>
            </div>
            
            <div className="form-group">
              <label htmlFor="highSystolic">{t('labels.highSystolic')}:</label>
              <input  type="number" 
                      className={this.state.highSystolicClass}
                      id="highSystolic" 
                      value={this.state.highSystolic}
                      onChange={this.handleHighSystolicChange}
                      />
                <div className="invalid-feedback">
                {t('errors.lowPressure')}
                </div>
            </div>
            
            <button className="btn btn-primary btn-lg btn-block" type="submit">{t('labels.addPatient')}</button>
            <div className={this.state.formStatusClass}>
                {this.state.formStatus} 
            </div>
          </form>
        </div>
      </div>
      <div className="table-responsive">
         <table className="table table-bordered table-hover">
             <thead className="thead-dark">
             <tr>
                 <th>{t('labels.table.firstName')}</th>
                 <th>{t('labels.table.lastName')}</th>
                 <th>{t('labels.table.email')}</th>
                 <th>{t('labels.table.country')}</th>
                 <th>{t('labels.table.province')}</th>
                 <th>{t('labels.table.postalCode')}</th>
                 <th>{t('labels.table.lowSystolic')}</th>
                 <th>{t('labels.table.highSystolic')}</th>
                 <th>{t('labels.table.deletePatient')}</th>
               </tr>
             </thead>
           <tbody>
             {
               this.state.patients.map( patient => {
                 return (
                   <tr key={patient.firstName}>
                     <td>{patient.firstName}</td> 
                     <td>{patient.lastName}</td> 
                     <td>{patient.email}</td> 
                     <td>{patient.country}</td> 
                     <td>{patient.province}</td> 
                     <td>{patient.postalCode}</td> 
                     <td>{patient.lowSystolic}</td> 
                     <td>{patient.highSystolic}</td> 
                     <td><button onClick={this.deletePatient.bind(this, patient.id)} type="button" className="btn btn-danger">{t('labels.table.deletePatient')}</button></td> 
                   </tr>
                 )
               })
             }
           </tbody>
         </table>
      </div>
      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

const translatePatients = withTranslation()(Patients);

export default translatePatients;