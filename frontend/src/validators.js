const USZipRegex = /^([0-9]{5})(?:[-\s]*([0-9]{4}))?$/;
const CAZipRegex = /^([A-Z][0-9][A-Z])\s*([0-9][A-Z][0-9])$/;

export function validatePostalCode(country, postalCode){
    switch(country){
      case 'Canada': 
        if(CAZipRegex.test(postalCode)){
          return true;
        }         
        break;
      case 'USA': 
        if(USZipRegex.test(postalCode)){
          return true;
        }         
        break;
    }
    return false;
}