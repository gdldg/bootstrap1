// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    const { app, data } = context;
    
    if(!data.firstName){
      throw new Error('Patient must have a first name!');
    };
    const firstName = data.firstName;
    
    if(!data.lastName){
      throw new Error('Patient must have a last name!');
    };
    const lastName = data.lastName;
    
    if(!data.email){
      throw new Error('Email must not be empty!');
    };
    const email = data.email;
    
    if(!data.country){
      throw new Error('Country must not be empty!');
    };
    const country = data.country;
    if(!data.province){
      throw new Error('Province must not be empty!');
    };
    const province = data.province;
    
    if(!data.postalCode){
      throw new Error('Postal Code must not be empty!');
    };
    const postalCode = data.postalCode;
    
    if(!data.lowSystolic){
      throw new Error('Low systolic pressure must not be empty!');
    }
    
    if(!data.highSystolic){
      throw new Error('High systolic pressure must not be empty!');
    }
    
    const lowSystolic = data.lowSystolic;
    const highSystolic = data.highSystolic;
    
    if(Number(lowSystolic) > Number(highSystolic)){
      throw new Error('Low systolic pressure must be lower than High Systolic pressure');
    }
    
    const emails = await app.service('patients').find({
      query: {
        email: email
      }
    });
    
    if(emails.total > 0){
      throw new Error('Email already exists');
    };
    
    context.data = {
      firstName,
      lastName,
      email,
      country,
      province,
      postalCode,
      lowSystolic,
      highSystolic,
    }
    
    return context;
  };
}
